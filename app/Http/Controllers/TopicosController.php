<?php

namespace App\Http\Controllers;

use App\topicos;
use Illuminate\Http\Request;

class TopicosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('topicos');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('topicos');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $topicos = new topicos;
        $topicos->descricao = $request ->get('descricao');
        $topicos->user_id=auth::id();

        $topicos->save();
        return view('topicos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\topicos  $topicos
     * @return \Illuminate\Http\Response
     */
    public function show(topicos $topicos)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\topicos  $topicos
     * @return \Illuminate\Http\Response
     */
    public function edit(topicos $topicos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\topicos  $topicos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, topicos $topicos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\topicos  $topicos
     * @return \Illuminate\Http\Response
     */
    public function destroy(topicos $topicos)
    {
        //
    }
}
